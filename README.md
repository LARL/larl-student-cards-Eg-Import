# student-cards

PINES student card import utilities from school information management systems.

## Basic Usage

    ./import_student_data.pl

To be run on a server with direct access to the PostgreSQL server running Evergreen-ILS.

These arguments are also available.

    --file , File to load in single file mode
    --district , District prefix when loading one file
    --debug

## Background/Assumptions

This project has been developed to support a PINES library system who wishes to have
an automated process for importing student card accounts into PINES.  In this context
"student card" refers to a restricted patron account providing basic access to students
within a school district.

This fork of the PINES library system code has been customized by the Lake Agassiz Regional Library system to support some changes in how LARL will import data for schools.

In this project, each school district is the parent unit over a set of school units divided
by grade level (e.g., Pre-K, Elementary, Middle School, High School) which are geographically
located near particular library branches.  The library system structure is similar, with a 
"System" as the parent unit over its "Branches".

This script assumes the presence of a "Student Card" permissions profile within Evergreen.

## Package Prerequisites
These packages are needed.
 - libfile-rsync-perl
 - Text::CSV - need a newer version, so use cpan (cpan install Text::CSV)

## Installation

Run `student_card_db_schema.sql` with psql to create the required schema in the database.

The script is intended to run nightly via a cron job on a utility server.

## Setup

On a server with a direct connection to the database server, create a `~/.pgpass` file
with the connection information and credentials for the PostgreSQL instance.  Then,
create a `~/.pg_service.conf` file with something like the following:

    [evergreen]
    dbname=evergreen
    user=evergreen
    host=localhost

where `dbname` is the name of the database you're connecting to as `user` on `host`.

### Importing District and School Data

To add a school district to the database, do the following:

    insert into student_card.district (
        code, 
        name, 
        state_id,
        contact_name, 
        contact_email
    ) values (
        'MSD', -- this code will become the prefix to the student card barcode
        'My School District',
        9999,
        'Jane Admin',
        'sysadmin@myschooldistrict.com'
    );

The `code` value will be used for naming the remote export files and for prefixing the 
student card account barcode.

`name`, `contact_name`, and `contact_email` are visible only to Evergreen database
administrators.

To add the schools, insert each with something like the following:

    insert into student_card.school (
        district_id,
        name,
        state_id,
        grades,
        addr_street_1,
        addr_street_2,
        addr_city,
        addr_county,
        addr_state,
        addr_post_code,
        eg_perm_group,
        home_ou
    ) values (
        (select id from student_card.district where code = 'MSD'),
        'My School',
        1234,
        '9-12',
        '123 Any Avenue',
        'Suite 200',
        'My City',
        'My County',
        'My State',
        '12345',
        (select id from permission.grp_tree where name = 'Student Card'),
        (select id from actor.org_unit where shortname = 'BR1')
    );

`district_id` (integer) will refer to the `id` field on `student_card.district`.

`name` (text) will be added to the "Secondary Identification" field within the user account.

`state_id` (integer) is used to match student data within an import file to the correct school.

`grades` (text) and address data (text) is only visible to Evergreen database administrators.

`eg_perm_group` (integer) refers to the ids of available profiles in `permission.grp_tree`.

`home_ou` (integer) refers to the ids of organizational units in `actor.org_unit`. Deciding
which library should be assigned to which school is up to the local library/school systems.
       
### Importing District and School Data in Bulk

There is a script for importing a large number of districts and schools from a csv.

add_district_and_schools.pl


## Import File Format

Import files will need to be in CSV format with the following fields and header names in the
following order:

    School_ID (State-assigned numeric code)
    Student_ID (District-assigned unique ID)
    Status (ACTIVE or INACTIVE, Opt-In Flag)
    Student_FName
    Student_MName (Optional)
    Student_LName
    Student_DOB (YYYY-MM-DD)
    Student_Phone (555-555-5555)
    Student_SMS (Optional) (555-555-5555)
    Student_Email (Optional, but highly recommended to allow automated notices - this could also be the parent/guardian's email address)
    Physcial_Address_Street1
    Physcial_Address_Street2 (Optional)
    Physcial_Address_City
    Physcial_Address_County
    Physcial_Address_State
    Physcial_Address_Postal
    Mailing_Address_Street1
    Mailing_Address_Street2 (Optional)
    Mailing_Address_City
    Mailing_Address_County
    Mailing_Address_State
    Mailing_Address_Postal
    Parent_Guardian (Optional)
    Grade (Optional)
    Password (Optional)

Files must have the naming convention of the district code and a date string `PRE_YYYYMMDDHHMM`.  For example:

    MSD_202002111037

would be a file created by the MSD school district on February 11, 2020 at 10:37 a.m.


## Imports

Files will be exported from the school district's student management system (SMS) and conveyed by SFTP to the GPLS-hosted destination.
Login information will be provided at the time of setup.

Imports will be performed nightly via cron.  The script will log into the SFTP server and download any files with the
proper naming convention that have not already been downloaded.  Processed file names are recorded in the `student_card.import` table.

The import script checks each row in the CSV file, generates the library card barcode (district code + student ID) and checks
whether that barcode already exists in the Evergreen database.  If the barcode does not yet exist, the script creates the account with
the row data.  If the barcode exists, the script will update selected fields in the account with the import file's data.

The import_student_data.pl script hard codes the central sftp server location.

### OptOut File Imports

If a file has the keyword OptOut in the filename, the file will be treated as a list of accounts to delete.

    MSD_202202111037_OptOut

These accounts are purged from Evergreen and their PII is removed or anonymized.

Does this happen in account owes money or has items still checked out?

## Updated Data Fields

If an account already exists, these data fields will be updated.

- **Student_FName**
- **Student_MName**
- **Student_LName**
- **Student_DOB**
- **Student_Phone**
- **Parent_Guardian**
- **All Address Fields**
- Ident Type
- Ident Value
- Ident2 Value
- Home Branch
- Expire Date

## Field Mappings

Below are the mappings between the CSV Fields and the destination Evergreen database table columns.

- **School_ID** is used to match the student to the school information
- **Student_ID** along with the district code, becomes the library card barcode and is also available in `actor.usr.ident_value2`
- **State** Determins if student will be loaded/updated or purged.  Should be "ACTIVE" or "INACTIVE" to represent Opt-In or Opt-Out.
- **Student_FName** becomes the patron first name (`actor.usr.first_given_name`)
- **Student_MName** becomes the patron middle name (`actor.usr.second_given_name`)
- **Student_LName** becomes the patron last name (`actor.usr.family_name`)
- **Student_DOB** becomes the patron date of birth and is used to generate the initial patron password (`actor.usr.dob` and `actor.usr.passwd`)
- **Student_Phone** becomes the patron phone number
- **Student_SMS** because default sms number for hold pickup notices
- **Student_Email** becomes `actor.usr.email`
- **Physical_Address_Street1** becomes the first street entry on the patron physical address (`actor.usr_address.street1`)
- **Physical_Address_Street2** becomes the second street entry on the patron physical address (`actor.usr_address.street2`)
- **Physical_Address_City** becomes the city on the patron physical address (`actor.usr_address.city`)
- **Physical_Address_County** becomes the county on the patron physical address (`actor.usr_address.county`) (school county is used if county is not provided)
- **Physical_Address_State** becomes the state on the patron physical address (`actor.usr_address.state`)
- **Physical_Address_Postal** becomes the ZIP code on the patron physical address (`actor.usr_address.post_code`)
- **Mailing_Address_Street1** becomes the first street entry on the patron mailing address (`actor.usr_address.street1`)
- **Mailing_Address_Street2** becomes the second street entry on the patron mailing address (`actor.usr_address.street2`)
- **Mailing_Address_City** becomes the city on the patron mailing address (`actor.usr_address.city`)
- **Mailing_Address_County** becomes the county on the patron mailing address (`actor.usr_address.county`) (school county is used if county is not provided)
- **Mailing_Address_State** becomes the state on the patron mailing address (`actor.usr_address.state`)
- **Mailing_Address_Postal** becomes the ZIP code on the patron mailing address (`actor.usr_address.post_code`)
- **Parent_Guardian** becomes the parent/guardian field (`actor.usr.guardian`).  If empty, the school name is entered in this field.
- **Grade** This is appended to "grade" and entered in the Name Keywords field (`actor.usr.name_keywords`)
- **Password** This is used as the initial password if supplied.  Otherwise the DOB is used.

### State Field Notes
Only current students should be included in the export.  We don't want information on students that have moved or graduated.  If OPTOUT file is being used, then set state to ACTIVE for all entries.

If district wants to only send one file for Opt-In and Opt-out then the state field can be use.

Only send School_ID, Student_ID and State for Opt-Out Students to minimize data shared.

### Address Fields Notes
If Physical and Mailing address are duplicates, then only one address with both flags set is added.

If Mailing address is distinct, then a seperate mailing address will be saved.

Mailing_Address fields can be left blank, or be duplicates of Physical_Address fields if there is only one address.
