#!/usr/bin/perl

# (C) Copyright 2019-2022 Georgia Public Library Service
# Chris Sharp <csharp@georgialibraries.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# This script is for importing student data from a CSV
# exported from a student information system (SIS).
#
use warnings;
use strict;
use Text::CSV qw/ csv /;
use DBI;
use Try::Tiny;
use Date::Parse;
use File::Rsync;
use Getopt::Long;

# 0 = no stdout messages
# 1 = basic
# 2 = detailed
my $debug = 0;
my $prefix;
my @listed_files;
# TODO: develop a reliable query for getting the 
# student card - or use YAOUS or something to set
# the profile.
my $profile = 17; # Student Card
my $district_id = 0;
my $error_message;
my @bad_student_ids;
my $sth_district;
my $remote_server = 'stompro@virt-SchoolFtp1.larl.org';
my $local_dir = "/tmp";
GetOptions( "file=s" => \@listed_files,
            "district=s" => \$prefix,
            "debug" => \$debug
);

# set up the DB connection
my $dbh = DBI->connect
(
    "dbi:Pg:service=evergreen",
    undef,
    undef,
    {
        AutoCommit => 0,
        RaiseError => 1,
        PrintError => 0
    }
) or die DBI->errstr;

# set up the SQL queries

# get the districts
my $district_sql = "select * from student_card.district where active = true";

# get a specific district when in single-file mode
my $single_district_sql = "select * from student_card.district where code = ?";

# gather previously-processed filenames - populated below
my $prev_files_sql;

# check that user exists
my $check_barcode_sql = "select 1 from actor.card where barcode = ?";

# insert user
my $insert_user_sql = qq(
insert into actor.usr (
    profile,
    usrname,
    email,
    passwd,
    ident_type,
    ident_value,
    ident_type2,
    ident_value2,
    first_given_name,
    second_given_name,
    family_name,
    day_phone,
    home_ou,
    dob,
    expire_date,
    juvenile,
    guardian,
    name_keywords,
    alert_message
) values (
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?
));

my $school_data_sql = q(
    select home_ou, name, addr_county, eg_perm_group
       from student_card.school 
       where district_id = ? and state_id = ?);

my $insert_address_sql = qq(
insert into actor.usr_address (
    usr,
    address_type,
    street1,
    street2,
    city,
    county,
    state,
    country,
    post_code
) values (
    (select id from actor.usr where usrname = ?),
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    'US',
    ?
)
returning id
);

my $insert_address_byusrid_sql = qq(
insert into actor.usr_address (
    usr,
    address_type,
    street1,
    street2,
    city,
    county,
    state,
    country,
    post_code
) values (
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    ?,
    'US',
    ?
)
returning id
);

my $link_addr_sql = qq(
update actor.usr
set mailing_address = (
    select id 
    from actor.usr_address
    where usr in (
        select id
        from actor.usr
        where usrname = ?
    )
)
where usrname = ?
);

my $link_mailing_addr_sql = q(
update actor.usr
set mailing_address = $1
where usrname = $2
);

my $link_physical_addr_sql = q(
update actor.usr
set billing_address = $1
where usrname = $2
);

my $link_mailing_addr_byusrid_sql = q(
update actor.usr
set mailing_address = $1
where id = $2
);

my $link_physical_addr_byusrid_sql = q(
update actor.usr
set billing_address = $1
where id = $2
);

#Delete address, based on user id and address id
my $delete_addr_sql = q(
    delete from actor.usr_address
    where usr=$1 and id=$2;
);

my $insert_card_sql = qq(
insert into actor.card (
    usr,
    barcode
) values(
    (select id from actor.usr where usrname = ?),
    ?
));

my $link_card_sql = qq(
update actor.usr
set card = (
    select id
    from actor.card
    where barcode = ?
)
where usrname = ?
and not deleted
);

my $insert_import_sql = qq(
insert into student_card.import (
    district_id,
    filename,
    error_message
    ) values (
    ?,
    ?,
    ?
));

## Insert/Update user setting/preference
my $insert_usr_setting = q(
insert into actor.usr_setting as aus(
    usr,
    name,
    value
  ) values (
    $1,
    $2,
    $3
  )
  on conflict on constraint usr_once_per_key
  do update set value=$3
  where aus.value!=$3
);

## Insert/Update stat cat
my $insert_stat_cat = q(
insert into actor.stat_cat_entry_usr_map as asceum(
    target_usr,
    stat_cat, -- actor.stat_cat(id)
    stat_cat_entry -- string value
  ) values (
    $1,
    (select id from actor.stat_cat where name=$2),
    $3
  )
  on conflict on constraint sc_once_per_usr
  do update set stat_cat_entry=$3
  where asceum.stat_cat_entry!=$3
);

## Insert Standing Penalty Note
my $insert_standing_penalty_note = q(
    insert into actor.usr_standing_penalty
    (org_unit,usr,standing_penalty,staff,set_date,stop_date,note)
    values (
        1,
        $1, --usr
        21, -- Silent Note
        1,
        now(),
        now(), --Archive it to start with
        $2 -- note
    )
);

# update user
my $get_user_id_sql = qq (
select usr
from actor.card
where barcode = ?
);

my $get_mailing_addr_sql = qq(
select mailing_address
from actor.usr
where id = ?
);

my $get_physical_addr_sql = qq(
select billing_address
from actor.usr
where id = ?
);

my $update_user_sql = q(
update actor.usr set
    first_given_name = $1,
    second_given_name = $2,
    family_name = $3,
    dob = $4,
    day_phone = $5,
    guardian = $6,
    name_keywords = $7,
    ident_type = $8,
    ident_value = $9,
    ident_value2 = $10,
    home_ou = $11,
    expire_date = $12
where 
    id = $13
    and
      ( first_given_name != $1 or
        second_given_name != $2 or
        family_name != $3 or
        dob != $4 or
        day_phone != $5 or
        guardian != $6 or
        name_keywords != $7 or
        ident_type != $8 or
        ident_value != $9 or
        ident_value2 != $10 or
        home_ou != $11 or
        expire_date != $12
      )
);

my $update_address_sql = q(
update actor.usr_address set
    address_type = $1,
    street1 = $2,
    street2 = $3,
    city = $4,
    county = $5,
    state = $6,
    post_code = $7
where 
    id = $8
    and (
      address_type != $1 or
      street1 != $2 or
    street2 != $3 or
    city != $4 or
    county != $5 or
    state != $6 or
    post_code != $7
    )
);


# delete user
my $delete_user_sql = qq(
select actor.usr_delete(?, 0)
);

sub calculate_expire_date {
    my @now = localtime();
    my ($now_year, $now_month) = ($now[5] + 1900, $now[4] + 1);
    my $expire_year = $now_year;
    if ($now_month > 6) { # any student registered before end of June gets expire date the following 9/15
        $expire_year += 1;
    }
    my $expire_date = $expire_year . "-10-15";
    return $expire_date;
}

sub calculate_password {
    my $dob = shift;
    my @elements = split('-', $dob);
    my ($dob_month, $dob_year) = ($elements[1], $elements[0]);
    #if ($dob_month < 10) {
    #    $dob_month = "0" . $dob_month;
    #}
    my $password = $dob_month . $dob_year;
    return $password;
}

sub get_remote_file_list {
        my $district_code = shift;
        my @remote_files;
        my $rsync = File::Rsync->new(archive => 1);
        my @long_listing = $rsync->list( src => "$remote_server:~/schooldata/$district_code/");
        print "@long_listing \n " if $debug;
        shift @long_listing; # remove the "Files" directory
        print "@long_listing \n" if $debug;
        for my $listing (@long_listing) {
                chomp($listing);
                my @list = split(' ', $listing);
                push(@remote_files, $list[-1]);
        }
        return @remote_files;
}

sub get_district_id_from_prefix {
    my $prefix = shift;
    $sth_district = $dbh->prepare($single_district_sql);
    $sth_district->execute($prefix);
    while (my $district = $sth_district->fetchrow_hashref) {
        $district_id = $district->{id};
    }
    $sth_district->finish();
    die "No district with code $prefix." unless $district_id;
    return $district_id;
}


if (@listed_files && $prefix) {
        get_district_id_from_prefix($prefix);
        process_files_per_district($prefix, \@listed_files);
} else {
    # go out and get the files we need
    $sth_district = $dbh->prepare($district_sql);
    $sth_district->execute();
    while (my $district = $sth_district->fetchrow_hashref) {
        my @retrieved_files = ();
        $district_id = $district->{id};
        $prev_files_sql = "select filename from student_card.import where district_id = $district_id";
        $prefix = $district->{code};
        if ($debug) {
            print "Now processing files for " . $prefix . "\n";
        }
        
        # get a listing of files on the remote server for the given district
        my @remote_files = get_remote_file_list($prefix);
        
        if ($debug == 2) {
            print "Remote File Listing: @remote_files\n";
        }
        ## walk through the remote files and filter out any that do
        ## not meet our naming requirements
        foreach my $file (@remote_files) {
            # XXX hard-coding the "OptOut" string here - maybe make it a setting? 
            if ($file =~ /${prefix}(OptOut)?_\d{12}/) { 
                my $previous = $dbh->selectcol_arrayref($prev_files_sql);
                # skip files we have already retreived
                unless (grep /$file/, @$previous) {
                    if ($debug) {
                        print "Getting $file\n";
                    }
                    mkdir "$local_dir/$prefix" unless (-d "$local_dir/$prefix");
                    my $rsync = File::Rsync->new(
                        compress => 1
                    );
                    $rsync->exec( 
                        src  => "$remote_server:~/schooldata/$prefix/$file",
                        dest => "$local_dir/$prefix/$file"
                    );
                    my $errors = $rsync->err;
                    print "@$errors\n" if $errors;
                    push (@retrieved_files, "$local_dir/$prefix/$file");
                } else {
                    if ($debug) {
                        print "$file already retreived\n";
                    }
                }
            }
        }
        process_files_per_district($prefix, \@retrieved_files);
    }
    $sth_district->finish;
}

sub process_files_per_district {
    my ($prefix, $retrieved_files) = @_;
    my @sorted_files = sort @$retrieved_files;
    foreach my $csv_file (@sorted_files) {
        print "Processing $csv_file\n";
        my $imports = 0;
        my $updates = 0;
        my $deletes = 0;
        my $skips   = 0;
        my @exceptions = ();
        my $opt_out;
        $opt_out = 1 if $csv_file =~ /OptOut/;
        # a couple of options here - we can assume that the headers are correct
        # and use headers => "auto" here, which will fail to work if there's a
        # typo in the header names, or we can do headers => "skip" and trust
        # that the export follows our prescribed order.
        # For now, we'll trust correct order over correct header names.
        my $student_entries = csv (in => "$csv_file", headers => "skip", empty_is_undef => 1)
            or die Text::CSV->error_diag;
        foreach my $student (@$student_entries) {
            next unless $student;
            # set up variables
            my $school_id       = @$student[0];
            my $student_id      = @$student[1];
            my $enrolled        = @$student[2];
            my $first_name      = @$student[3];
            my $middle_name     = @$student[4] || '';
            my $last_name       = @$student[5];
            my $dob             = @$student[6];
            my $phone           = @$student[7];
            my $sms             = @$student[8] || '';
            my $email           = @$student[9];

            my $street1         = @$student[10];
            my $street2         = @$student[11] || '';
            my $city            = @$student[12];
            my $county          = @$student[13];
            my $state           = @$student[14];
            my $post_code       = @$student[15];

            my $mailing_street1        = @$student[16];
            my $mailing_street2        = @$student[17] || '';
            my $mailing_city           = @$student[18];
            my $mailing_county         = @$student[19];
            my $mailing_state          = @$student[20];
            my $mailing_post_code      = @$student[21];
            
            my $parent_guardian = @$student[22];
            my $grade           = @$student[23];
            my $importpw        = @$student[24];


            #Handle strings for enrolled values
            $enrolled = 1 if uc($enrolled) eq 'ACTIVE';
            $enrolled = 0 if uc($enrolled) eq 'INACTIVE';

            #If the entry is not enrolled, set opt_out.
            if ($enrolled) {
                $opt_out = 0;
            }
            else {
                $opt_out = 1;
            }
        
            my $barcode         = $prefix . $student_id;
            my $username        = $barcode;
            
            #set some dummy values to keep exception from being triggered
            #if the inactive/opt out data only includes school_id and student_id.
            if($opt_out) {
               $dob = '1970-01-01' if !$dob;
               $first_name = 'opt-out' if !$first_name;
               $last_name = 'opt-out' if !$last_name;
               $street1 = 'opt out user' if !$street1;
               $city = 'opt-out' if !$city;
               $state = 'opt-out' if !$state;
               $post_code = 'opt-out' if !$post_code; 
               $mailing_street1 = 'opt out user' if !$mailing_street1;
               $mailing_city = 'opt out user' if !$mailing_city;
            }
            my $password;
            $password        = calculate_password($dob) unless $opt_out or $importpw;
            $password        = $importpw if $importpw;
            my $alert           = qq(Student Card Account - please do not edit information or renew card, student's parent or guardian should update their school registration information to update account.);     
            my $ident_type      = 3; # Other
            my $ident_value     = $student_id;
            my $expire_date     = calculate_expire_date;
            my $juvenile        = "true";
            my $name_keywords;

            
            if ($grade && $grade =~ /^\d+$/) {
                if ($grade == 0) {
                    $name_keywords = "GradeK";
                } elsif ($grade < 0) {
                    $name_keywords = "GradePK";
                } elsif ($grade < 10) {
                    $name_keywords = "Grade0" . $grade;
                } else {
                    $name_keywords = "Grade" . $grade;
                }
            } elsif ($grade) {
                $name_keywords = $grade;
            }
            # figure out how to get the home ou from the DB based
            # on the school ID
            my $sth_school_data = $dbh->prepare($school_data_sql);
            $sth_school_data->execute($district_id, $school_id);
            my @school_result = $sth_school_data->fetchrow_array;
            $sth_school_data->finish;
            my $home_branch = $school_result[0];
            my $school_name = $school_result[1];
            my $school_county = $school_result[2];
            my $ident2_value = $school_name;
            my $eg_perm_group = $school_result[3]; #Perm group id from school data
            $profile = $eg_perm_group if $eg_perm_group; #Use permission group from school data if it exists.
            my $diff_addresses = 0;
            ### Seperate Mailing Address in export?
            if( !($street1 eq $mailing_street1 &&
                $street2 eq $mailing_street2 &&
                $city eq $mailing_city &&
                $state eq $mailing_state &&
                $post_code eq $mailing_post_code)
            ) {
                print("\n\n$student_id - Mailing address different from physical address\n") if $debug==2;
                $diff_addresses = 1;
            }
            
            # make sure we have a usable county entry
            # if not, add from the school data
            if (!$county || $county =~ m/(\d|MN)/) {
                $county = $school_county;
            }
            if (!$mailing_county || $mailing_county =~ m/(\d|MN)/) {
                $mailing_county = $school_county;
            }
            # check that we have the required data for the student
            if (!$parent_guardian) {
                $parent_guardian = $school_name;
            }
            
            #If parent/Guardian data is a phone number, add some context info
            if($parent_guardian =~ m/\d{3}-\d{3}-\d{4}/) {
              $parent_guardian = "Parent Number: $parent_guardian";
            }
            
            # write out an exceptions file
            my $no_import = 0;
            unless (
                $school_id && 
                $student_id &&
                $first_name &&
                $last_name &&
                $dob &&
                $street1 &&
                $city &&
                $state &&
                $post_code &&
                $home_branch ) {
                if ($debug == 2) {
                    print "Student $student_id is an exception.\n";
                }
                push @exceptions, $student;
                $no_import = 1;
            }
            unless ($no_import) {
                # check if we already have the student account
                my $sth_check_barcode = $dbh->prepare($check_barcode_sql);
                $sth_check_barcode->execute($barcode);
                my $already_imported = $sth_check_barcode->fetchrow_array;
                $sth_check_barcode->finish;
                my $sth_user_id = $dbh->prepare($get_user_id_sql);
                $sth_user_id->execute($barcode);
                my @user_result = $sth_user_id->fetchrow_array;
                $sth_user_id->finish;
                my $user_id = $user_result[0];

                if ($already_imported && !$opt_out) {
                    #Grab physical address id
                    my $sth_addr = $dbh->prepare($get_physical_addr_sql);
                    $sth_addr->execute($user_id);
                
                    my @addr_result = $sth_addr->fetchrow_array;
                
                    $sth_addr->finish;
                    my $addr_physical_id = $addr_result[0];

                    #Grab mailing address id
                    $sth_addr = $dbh->prepare($get_mailing_addr_sql);
                    $sth_addr->execute($user_id);
                
                    @addr_result = $sth_addr->fetchrow_array;
                
                    $sth_addr->finish;
                    my $addr_mailing_id = $addr_result[0];
                    my $addr_state = 0;
                    if($addr_physical_id != $addr_mailing_id){
                        #Patron currently has two different addresses
                        $addr_state = 1;
                    }

                    my @update_user_data = (
                        $first_name,
                        $middle_name,
                        $last_name,
                        $dob,
                        $phone,
                        $parent_guardian,
                        $name_keywords,
                        $ident_type,
                        $ident_value,
                        $ident2_value,
                        $home_branch,
                        $expire_date,
                        $user_id
                    );
                    my @update_addr_data = (
                        'PHYSICAL',
                        $street1,
                        $street2,
                        $city,
                        $county,
                        $state,
                        $post_code,
                        $addr_physical_id
                    );
                    my @update_sms_data = (
                        $user_id,
                        'opac.default_sms_notify',
                        '"'.$sms.'"'
                    );
                    my @update_holdnotify_data = (
                        $user_id,
                        'opac.hold_notify',
                        '"email:sms"'
                    );
                    # MN System stat cat
                    my @update_statcat_zero_data = (
                        $user_id,
                        'Minnesota System Libraries',
                        'ZZZ Student Card Account (Not For Staff Use)'
                    );
                    # Student Account District stat cat
                    my @update_statcat_one_data = (
                        $user_id,
                        'Student Account District',
                        $prefix
                    );
                    # Student Account School stat cat
                    my @update_statcat_two_data = (
                        $user_id,
                        'Student Account School',
                        $school_name
                    );
                    # Student Account Grade stat cat
                    my @update_statcat_three_data = (
                        $user_id,
                        'Student Account Grade',
                        $grade
                    );
                    
                    my @insert_standing_penalty_data = (
                        $user_id,
                        'Student Card Data updated:'
                    );

                    my $data_updated = 0;

                    # do the update
                    if ($debug) {
                        print "\nUpdating $barcode\n";
                    }
                    try {
                        # update user
                        if ($debug == 2) {
                            print "Update user data: @update_user_data\n";
                        }
                        my $sth_update_user = $dbh->prepare($update_user_sql);
                        if ($sth_update_user->execute(@update_user_data) eq '0E0'){
                            print "  User data not changed so not updated\n" if $debug == 2;
                        }
                        else {
                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - User Data';
                        }
                        
                        ## Different address situations.
                        ### Currently have 1 address, 1 address to update.
                        ### Currently have 2 addresse, 2 addresses to update
                        ### Currently have 1 address, 2 addresses to update. -- add one address
                        ### Currently have 2 addresses, 1 address to update.  -- Delete one address
                        ## $addr_state has 0 for 1 current address or 1 for 2 current addresses
                        ## $diff_addresses has 0 for 1 address, 1 for 2 addresses
                        # update address

                        #1 current and 1 new address
                        my $sth_update_addr = $dbh->prepare($update_address_sql);
                        my $sth_delete_addr = $dbh->prepare($delete_addr_sql);

                        if(!$diff_addresses and !$addr_state){
                            if ($debug == 2) {
                                print "Update address data: @update_addr_data\n";
                            }
                            
                            if ($sth_update_addr->execute(@update_addr_data) eq '0E0'){
                                print "  Address data not changed so not updated\n" if $debug == 2;
                            }else {
                                $data_updated=1;
                                $insert_standing_penalty_data[1] .= ' - Address Data';
                            }
                        } elsif ($diff_addresses and $addr_state){  #Two addresses
                     
                            if ($debug == 2) {
                                print "Update Physical address data: @update_addr_data\n";
                            }
                            
                            if ($sth_update_addr->execute(@update_addr_data) eq '0E0'){
                                print "  Address data not changed so not updated\n" if $debug == 2;
                            }else {
                                $data_updated=1;
                                $insert_standing_penalty_data[1] .= ' - Address Data (Physical)';
                            }

                            my @update_addr_data = (
                                'MAILING',
                                $mailing_street1,
                                $mailing_street2,
                                $mailing_city,
                                $mailing_county,
                                $mailing_state,
                                $mailing_post_code,
                                $addr_mailing_id
                            );

                            if ($debug == 2) {
                                print "Update mailing address data: @update_addr_data\n";
                            }
                            
                            if ($sth_update_addr->execute(@update_addr_data) eq '0E0'){
                                print "  Address data not changed so not updated\n" if $debug == 2;
                            }else {
                                $data_updated=1;
                                $insert_standing_penalty_data[1] .= ' - Address Data (Mailing)';
                            }


                        } elsif ($diff_addresses and !$addr_state){ #Have 1, 2 addresses to update
                            #have to add a mailing address
                            if ($debug == 2) {
                                print "Update Physical address data: @update_addr_data\n";
                            }
                            
                            if ($sth_update_addr->execute(@update_addr_data) eq '0E0'){
                                print "  Address data not changed so not updated\n" if $debug == 2;
                            }else {
                                $data_updated=1;
                                $insert_standing_penalty_data[1] .= ' - Address Data (Physical)';
                            }
                            
                            ##Add mailing address and link
                            my @addr_data_mailing = (
                                $user_id,
                                'MAILING',
                                $mailing_street1,
                                $mailing_street2,
                                $mailing_city,
                                $mailing_county,
                                $mailing_state,
                                $mailing_post_code
                            );

                            if ($debug == 2) {
                                print "Insert Mailing address data: @addr_data_mailing\n";
                            }

                            my $sth_insert_addr = $dbh->prepare($insert_address_byusrid_sql);
                            $sth_insert_addr->execute(@addr_data_mailing);

                            my $addr_id = $sth_insert_addr->fetch()->[0];

                            my @link_addr_data_byusrid = (
                                $addr_id,
                                $user_id
                            );
                            my $sth_link_addr = $dbh->prepare($link_mailing_addr_byusrid_sql);
                            $sth_link_addr->execute(@link_addr_data_byusrid);

                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - Address Data (Added Mailing)';


                        } elsif (!$diff_addresses and $addr_state){ #Have 2, 1 address to update
                            #Have to delete one address, delete the mailing address
                            if ($debug == 2) {
                                print "Update address data: @update_addr_data\n";
                            }
                            
                            if ($sth_update_addr->execute(@update_addr_data) eq '0E0'){
                                print "  Address data not changed so not updated\n" if $debug == 2;
                            }else {
                                $data_updated=1;
                                $insert_standing_penalty_data[1] .= ' - Address Data (Physical)';
                            }

                            #delete mailing address and re-link the mailing to match the physical
                            if ($debug == 2) {
                                print "Delete mailing address: $addr_mailing_id\n";
                            }
                             ### Link mailing to physical
                            my @link_addr_data_byusrid = (
                                $addr_physical_id,
                                $user_id
                            );
                            my $sth_link_addr = $dbh->prepare($link_mailing_addr_byusrid_sql);
                            $sth_link_addr->execute(@link_addr_data_byusrid);
                            
                            #Delete Mailing address after unlink
                            my @delete_addr_data = (
                                $user_id,
                                $addr_mailing_id
                            );

                            $sth_delete_addr->execute(@delete_addr_data);

                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - Address Data (Deleted Mailing)';
                        }
                    
                        # update sms notify number
                        if ($debug == 2 && $sms) {
                            print "Update sms phone data: @update_sms_data\n";
                        }
                        my $sth_update_setting = $dbh->prepare($insert_usr_setting);
                        if ($sms) {
                          if ($sth_update_setting->execute(@update_sms_data) eq '0E0'){
                            print "  SMS data not changed so not updated\n" if $debug == 2;
                          }
                          if ($sth_update_setting->execute(@update_holdnotify_data) eq '0E0'){
                            print "  Hold Notify data not changed so not updated\n" if $debug == 2;
                          } else {
                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - Hold Notify';
                            }   
                        }
                        
                        # update stat-cats - zero
                        if ($debug == 2) {
                            print "Update Stat Cat data: @update_statcat_zero_data\n";
                        }
                        my $sth_update_statcat = $dbh->prepare($insert_stat_cat);
                        if ($sth_update_statcat->execute(@update_statcat_zero_data) eq '0E0'){
                          print "  Stat Cat not changed so not updated\n" if $debug == 2;
                        }else {
                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - Stat Cat (system)';
                        }

                        # update stat-cats - One
                        if ($debug == 2) {
                            print "Update Stat Cat data: @update_statcat_one_data\n";
                        }
                        if ($sth_update_statcat->execute(@update_statcat_one_data) eq '0E0'){
                          print "  Stat Cat not changed so not updated\n" if $debug == 2;
                        }else {
                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - Stat Cat (District)';
                        }
                        
                        # update stat-cats - Two
                        if ($debug == 2) {
                            print "Update Stat Cat data: @update_statcat_two_data\n";
                        }
                        if ($sth_update_statcat->execute(@update_statcat_two_data) eq '0E0'){
                          print "  Stat Cat not changed so not updated\n" if $debug == 2;
                        }else {
                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - Stat Cat (School)';
                        }
                        
                        # update stat-cats - Three
                        if ($debug == 2) {
                            print "Update Stat Cat data: @update_statcat_three_data\n";
                        }
                        if ($sth_update_statcat->execute(@update_statcat_three_data) eq '0E0'){
                          print "  Stat Cat not changed so not updated\n" if $debug == 2;
                        }else {
                            $data_updated=1;
                            $insert_standing_penalty_data[1] .= ' - Stat Cat (Grade)';
                        }

                        my $sth_insert_standing_penalty = $dbh->prepare($insert_standing_penalty_note);
                        if($data_updated){
                            ### Enter a silent log note
                            #$insert_standing_penalty_note
                            if ($debug == 2) {
                                print "Insert silent note log: @insert_standing_penalty_data\n";
                            }
                            $sth_insert_standing_penalty->execute(@insert_standing_penalty_data);
                        }

                        $dbh->commit;
                        $sth_update_user->finish;
                        $sth_update_addr->finish;
                        $sth_update_setting->finish;
                        $sth_update_statcat->finish;
                        $sth_insert_standing_penalty->finish;
                        $updates++;
                    } catch {
                        warn "Transaction aborted because $_";
                        eval { $dbh->rollback };
                    };
     
                } elsif ($already_imported && $opt_out ) {
                    if ($debug) {
                        print "\nDeleting $barcode\n";
                    }
                    try {
                        # delete user
                        my $sth_delete_user = $dbh->prepare($delete_user_sql);
                        $sth_delete_user->execute($user_id);
                        $dbh->commit;
                        $sth_delete_user->finish;
                        $deletes++;
                    } catch {
                        warn "Transaction aborted because $_";
                        eval { $dbh->rollback };
                    };
                } elsif (!$already_imported && $opt_out) {
                    # do nothing, but count the opt out
                    if ($debug) {
                        print "\nSkipping $barcode\n";
                    }
                    $skips++;
                    next;
                } else {
                    # import user
                    my @user_data = (
                        $profile, 
                        $username, 
                        $email,
                        $password,
                        $ident_type,
                        $ident_value,
                        $ident_type,
                        $ident2_value,
                        $first_name,
                        $middle_name,
                        $last_name,
                        $phone,
                        $home_branch,
                        $dob,
                        $expire_date,   
                        $juvenile,
                        $parent_guardian,
                        $name_keywords,
                        $alert
                    );
                    my @addr_data_physical = (
                        $username,
                        'PHYSICAL',
                        $street1,
                        $street2,
                        $city,
                        $county,
                        $state,
                        $post_code,
                    );

                    my @card_data = (
                        $username,
                        $barcode
                    );
                    my @link_card_data = (
                        $barcode,
                        $username
                    );
                    my @link_addr_data = (
                        $username,
                        $username
                    );
                    
                    
                    
                   # do the import here
                    if ($debug) {
                        print "\nImporting $barcode\n";
                        }
                        try {
                            # insert user
                            if ($debug == 2) {
                                print "Inserting user data: @user_data\n";
                            }
                            my $sth_insert_user = $dbh->prepare($insert_user_sql);
                            $sth_insert_user->execute(@user_data);
                            
                            # insert address
                            if ($debug == 2) {
                                print "Inserting Physical/Mailing address data: @addr_data_physical\n";
                            }
                            my $sth_insert_addr = $dbh->prepare($insert_address_sql);
                            $sth_insert_addr->execute(@addr_data_physical);

                            my $addr_id = $sth_insert_addr->fetch()->[0];

                            $link_addr_data[0]=$addr_id;

                            # link address back to the user
                            my $sth_link_addr = $dbh->prepare($link_physical_addr_sql);
                            $sth_link_addr->execute(@link_addr_data);
                                
                            $sth_link_addr = $dbh->prepare($link_mailing_addr_sql);
                            $sth_link_addr->execute(@link_addr_data);
                            
                            if($diff_addresses){
                                ## Different Addresses - First is Physical, second is mailing
                                # insert address
                                my @addr_data_mailing = (
                                    $username,
                                    'MAILING',
                                    $mailing_street1,
                                    $mailing_street2,
                                    $mailing_city,
                                    $mailing_county,
                                    $mailing_state,
                                    $mailing_post_code,
                                );

                                if ($debug == 2) {
                                    print "Two addresses Inserting Mailing address data: @addr_data_mailing\n";
                                }
                                
                                $sth_insert_addr->execute(@addr_data_mailing);

                                my $addr_id = $sth_insert_addr->fetch()->[0];

                                $link_addr_data[0]=$addr_id;
                                my $sth_link_addr = $dbh->prepare($link_mailing_addr_sql);
                                $sth_link_addr->execute(@link_addr_data);
                            }

                            # insert card
                            if ($debug == 2) {
                                print "Inserting card data: @card_data\n";
                            }
                            my $sth_insert_card = $dbh->prepare($insert_card_sql);
                            $sth_insert_card->execute(@card_data);
                            # link card back to the user
                            my $sth_link_card = $dbh->prepare($link_card_sql);
                            $sth_link_card->execute(@link_card_data);
                            
                            ### Grab user id to simplify more inserts
                            my $sth_user_id = $dbh->prepare($get_user_id_sql);
                            $sth_user_id->execute($barcode);
                            my @user_result = $sth_user_id->fetchrow_array;
                            $sth_user_id->finish;
                            my $user_id = $user_result[0];
                            
                            my @update_sms_data = (
                              $user_id,
                              'opac.default_sms_notify',
                              '"'.$sms.'"'
                            );

                            my @update_holdnotify_data = (
                                $user_id,
                                'opac.hold_notify',
                                '"email:sms"'
                            );
                            my @update_holdnotify_data_default = (
                                $user_id,
                                'opac.hold_notify',
                                '"email"'
                            );
                            # MN System stat cat
                            my @update_statcat_zero_data = (
                                $user_id,
                                'Minnesota System Libraries',
                                'ZZZ Student Card Account (Not For Staff Use)'
                            );
                            # Student Account District stat cat
                            my @update_statcat_one_data = (
                                $user_id,
                                'Student Account District',
                                $prefix
                            );
                            # Student Account School stat cat
                            my @update_statcat_two_data = (
                                $user_id,
                                'Student Account School',
                                $school_name
                            );
                            # Student Account Grade stat cat
                            my @update_statcat_three_data = (
                                $user_id,
                                'Student Account Grade',
                                $grade
                            );
                            #Silent note log data
                            my @insert_standing_penalty_data = (
                                $user_id,
                                'Student Data inserted'
                            );
                            
                            # update sms notify number
                            if ($debug == 2 && $sms) {
                                print "Insert sms phone data: @update_sms_data\n";
                            }
                            my $sth_update_setting = $dbh->prepare($insert_usr_setting);
                            if ($sms) {
                              if ($sth_update_setting->execute(@update_sms_data) eq '0E0'){
                                print "  SMS data not changed so not updated\n" if $debug == 2;
                              }
                              if ($sth_update_setting->execute(@update_holdnotify_data) eq '0E0'){
                                print "  Hold Notify data not changed so not updated\n" if $debug == 2;
                              }
                            } else {
                              if ($sth_update_setting->execute(@update_holdnotify_data_default) eq '0E0'){
                                print "  Hold Notify Default data not changed so not updated\n" if $debug == 2;
                              }
                            }
                            # update stat-cats - Zero
                            if ($debug == 2) {
                                print "Update Stat Cat data: @update_statcat_zero_data\n";
                            }
                            my $sth_update_statcat = $dbh->prepare($insert_stat_cat);
                            if ($sth_update_statcat->execute(@update_statcat_zero_data) eq '0E0'){
                              print "  Stat Cat not changed so not updated\n" if $debug == 2;
                            }
                            # update stat-cats - One
                            if ($debug == 2) {
                                print "Update Stat Cat data: @update_statcat_one_data\n";
                            }
                            if ($sth_update_statcat->execute(@update_statcat_one_data) eq '0E0'){
                              print "  Stat Cat not changed so not updated\n" if $debug == 2;
                            }
                            # update stat-cats - two
                            if ($debug == 2) {
                                print "Update Stat Cat data: @update_statcat_two_data\n";
                            }
                            if ($sth_update_statcat->execute(@update_statcat_two_data) eq '0E0'){
                              print "  Stat Cat not changed so not updated\n" if $debug == 2;
                            }
                            # update stat-cats - three
                            if ($debug == 2) {
                                print "Update Stat Cat data: @update_statcat_three_data\n";
                            }
                            if ($sth_update_statcat->execute(@update_statcat_three_data) eq '0E0'){
                              print "  Stat Cat not changed so not updated\n" if $debug == 2;
                            }
                            

                            ### Enter a silent log note
                            #$insert_standing_penalty_note
                            my $sth_insert_standing_penalty = $dbh->prepare($insert_standing_penalty_note);
                            if ($debug == 2) {
                                print "Insert silent note log: @insert_standing_penalty_data\n";
                            }
                            $sth_insert_standing_penalty->execute(@insert_standing_penalty_data);

                            $dbh->commit;
                            $sth_insert_user->finish;
                            $sth_insert_addr->finish;
                            $sth_link_addr->finish;
                            $sth_insert_card->finish;
                            $sth_link_card->finish;
                            $sth_update_statcat->finish;
                            $sth_update_setting->finish;
                            $sth_insert_standing_penalty->finish;
                        } catch {
                            warn "Transaction aborted because $_";
                            eval { $dbh->rollback };
                        };
                        $imports++;
                    }
                
                }
            }
    
        # record the import in the DB
        my @file_path = split('/', $csv_file);
        my $filename = $file_path[-1]; # bare filename
        try {     
            my $sth_insert_import = $dbh->prepare($insert_import_sql);
            #TODO: error handling that gets pushed to the DB
            $sth_insert_import->execute($district_id, $filename, $error_message);
            $dbh->commit;
            $sth_insert_import->finish;
        } catch {
            warn "Transaction aborted because $_";
            eval { $dbh->rollback };
        };
        # write exceptions to a file
        my $exceptions_file = "$filename.exceptions";
        if (@exceptions) {
            my $exceptions_ref = \@exceptions;
            csv (in => $exceptions_ref, out => "/tmp/$exceptions_file")
                or die Text::CSV->error_diag;
            my $rsync = File::Rsync->new(archive => 1);
            $rsync->exec( 
                src => "/tmp/$exceptions_file",
                dest => "$remote_server:~/schooldata/${prefix}/exceptions/"
            ) or warn "Could not rsync $exceptions_file to $remote_server:~/schooldata/$prefix/exceptions/$exceptions_file\n";
        }
        print "\n\nImport statistics:\n\n\tImports: $imports\n\tUpdates: $updates\n\tOpt-Outs: $deletes\n\tSkips: $skips\n\tExceptions: " . scalar(@exceptions) . "\n";
        print "\n\n\tCreated exceptions file $exceptions_file.\n" if @exceptions;
    }
}
# clean up
$dbh->disconnect();

